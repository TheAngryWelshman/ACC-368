﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SignUp
    Inherits System.Web.UI.Page
    'instantiate connection object
    Dim conObj As conn = New conn
    Dim con As SqlConnection = conn.connect()

    Dim gGflag As Integer
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim cmdInsert As New SqlCommand("Insert Into [CustomerProject] (CustomerFName, CustomerLName, CustomerEmail, CustomerPhone, CustomerAddress, CustomerCity, CustomerState, CustomerZipCode, CustomerUsername, CustomerPassword) Values (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10)", con)
        Dim str As String
        gGflag = 0
        DataValidate()
        If gGflag > 0 Then
            Exit Sub
        End If

        With cmdInsert.Parameters
            .Clear()
            .AddWithValue("@p1", txtFName.Text)
            .AddWithValue("@p2", txtLName.Text)
            .AddWithValue("@p3", txtEmail.Text)
            .AddWithValue("@p4", txtPhone.Text)
            .AddWithValue("@p5", txtAddress.Text)
            .AddWithValue("@p6", txtCity.Text)
            .AddWithValue("@p7", txtState.Text)
            .AddWithValue("@p8", txtZip.Text)
            .AddWithValue("@p9", txtUsername.Text)
            .AddWithValue("@p10", txtPassword.Text)
        End With

        Try
            If con.State = ConnectionState.Closed Then con.Open()
            cmdInsert.ExecuteNonQuery()
            Response.Write("Record Added, Please Sign In!")
        Catch ex As Exception
            str = ex.Message
            If str.StartsWith("Violation of PRIMARY KEY") Then
                Response.Write("User name already taken")
            Else
                Response.Write(ex.Message)
            End If
        Finally
            con.Close()
        End Try

    End Sub
    Sub DataValidate()
        If txtPassword.Text = txtPasswordConfirm.Text Then
            gGflag += 0
        Else
            gGflag += 1
            Response.Write("Make Sure Your Password Is The Same")
        End If
        If txtFName.Text = Nothing Or txtFName.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter Your First Name, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtLName.Text = Nothing Or txtLName.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter Your Last Name, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtEmail.Text = Nothing Or txtEmail.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter Your Email, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtPhone.Text = Nothing Or txtPhone.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter Your Phone, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtState.Text = "" Or txtState.Text.Length > 3 Then
            gGflag += 1
            Response.Write("Please Enter Your State, And Make Sure It Is 2 Characters")
        End If
        If txtUsername.Text = Nothing Or txtUsername.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter A Username, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtPassword.Text = Nothing Or txtPassword.Text.Length > 25 Then
            gGflag += 1
            Response.Write("Please Enter A Password, And Make Sure It Is Less Than 25 Characters")
        End If
        If txtPasswordConfirm.Text = Nothing Then
            gGflag += 1
            Response.Write("Make Sure Your Password Is The Same")
        End If
        If txtZip.Text.Length > 5 Then
            gGflag += 1
            Response.Write("Please Enter A Zip Code That Is 5 Numbers Long")
        End If

    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtFName.Text = ""
        txtLName.Text = ""
        txtEmail.Text = ""
        txtPhone.Text = ""
        txtAddress.Text = ""
        txtCity.Text = ""
        txtZip.Text = ""
        txtUsername.Text = ""
        txtPassword.Text = ""
        txtPasswordConfirm.Text = ""
        txtState.Text = ""
    End Sub
    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Response.Redirect("SignIn.aspx")
    End Sub
End Class

CREATE TABLE CustomerProject(
CustomerID INT IDENTITY(1,1) PRIMARY KEY,
CustomerFName VARCHAR(255) NOT NULL,
CustomerLName VARCHAR(255) NOT NULL,
CustomerEmail VARCHAR(255),
CustomerPhone VARCHAR(13) NOT NULL,
CustomerAddress VARCHAR(255),
CustomerCity VARCHAR(255),
CustomerState CHAR(2),
CustomerZipCode INT,
CustomerUsername VARCHAR(255) UNIQUE NOT NULL,
CustomerPassword VARCHAR(255) NOT NULL
);

CREATE TABLE ReservationProject (
ReservationID INT IDENTITY(1,1) PRIMARY KEY,
TableDate DATE, 
TableTime TIME, 
TableGuestNumber INT, 
TableUsername VARCHAR(255), 
TableNumber INT,
FOREIGN KEY (TableUsername)
REFERENCES CustomerProject(CustomerUsername)
);

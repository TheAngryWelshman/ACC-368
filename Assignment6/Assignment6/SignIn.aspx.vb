﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SignIn
    Inherits System.Web.UI.Page
    'instantiate connection object
    Dim conObj As conn = New conn
    Dim con As SqlConnection = conn.connect()

    Dim gFlag1, gFlag2 As Integer
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        gFlag1 = 0
        DataValidate1()
        If gFlag1 > 0 Then
            Exit Sub
        End If


        Dim cmdPassword As New SqlCommand("select CustomerPassword from CustomerProject where CustomerUserName = @p1", con)

        With cmdPassword.Parameters
            .Clear()
            .AddWithValue("@p1", txtUsername.Text)
        End With

        Try
            If con.State = ConnectionState.Closed Then con.Open()
            txtPassHidden.Text = cmdPassword.ExecuteScalar
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            con.Close()

        End Try
        If txtPassword.Text = txtPassHidden.Text Then
            MsgBox("Login Granted")
            txtPassword.Text = ""
            MakeVisible()
        Else
            MsgBox("Invalid Username or Password")
        End If

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        gFlag2 = 0
        DataValidate2()
        If gFlag2 > 0 Then
            Exit Sub
        End If

        Dim cmdInsert As New SqlCommand("Insert Into ReservationProject (TableDate, TableTime, TableGuestNumber, TableUsername, TableNumber) Values (@p1, @p2, @p3, @p4, @p5)", con)
        With cmdInsert.Parameters
            .Clear()
            .AddWithValue("@p1", clnReserve.SelectedDate.Date)
            .AddWithValue("@p2", CStr(ddlTimes.SelectedValue))
            .AddWithValue("@p3", CInt(txtNumberPeople.Text))
            .AddWithValue("@p4", txtUsername.Text)
            .AddWithValue("@p5", CInt(txtTableNo.Text))
        End With

        Try
            If con.State = ConnectionState.Closed Then con.Open()
            cmdInsert.ExecuteNonQuery()
            Response.Write("Record Added")
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            con.Close()
        End Try

    End Sub

    Sub DataValidate1()
        If txtUsername.Text = "" Then
            gFlag1 += 1
            Response.Write("Please Enter A Username")
        End If
        If txtPassword.Text = "" Then
            gFlag1 += 1
            Response.Write("Please Enter A Password")
        End If
    End Sub
    Sub DataValidate2()
        If ddlTimes.SelectedIndex = -1 Then
            gFlag2 += 1
            Response.Write("Please Enter A Time")
        End If
        If txtTableNo.Text = "" Then
            gFlag2 += 1
            Response.Write("Please Enter A Table Number")
        End If
        If IsNumeric(txtTableNo.Text) = False Then
            gFlag2 += 1
            Response.Write("Please Enter A Numeric Table Number")
        End If
        If txtNumberPeople.Text = "" Then
            gFlag2 += 1
            Response.Write("Please Put How Many In Your Party")
        End If
        If clnReserve.SelectedDate < Today Then
            gFlag2 += 1
            Response.Write("Please Choose A Date After Today")
        End If
    End Sub
    Sub MakeVisible()
        lblDate.Visible = True
        lblNum.Visible = True
        txtNumberPeople.Visible = True
        clnReserve.Visible = True
        btnSubmit.Visible = True
        lblTableNo.Visible = True
        txtTableNo.Visible = True
        imgSeating.Visible = True
    End Sub

    Protected Sub Button1_Click1(sender As Object, e As EventArgs) Handles btnShowTimes.Click
        'Make the time ddl visible
        ddlTimes.Visible = True
        lblTime.Visible = True

        'Select all current reservations for the selected date and table number
        Dim sqlObj As sql = New sql

        Dim selNames() As String = {"TableTime"}
        Dim selTbl As String = "ReservationProject"
        Dim selCond(,) As String = {{"TableNumber = ", txtTableNo.Text}, {"AND Tabledate = ", clnReserve.SelectedDate.Date}}

        Dim dtReservations As DataTable = sql.selectDT(selNames, selTbl, selCond)

        ddlHidden.DataSource = dtReservations.DefaultView
        ddlHidden.DataTextField = "TableTime"
        ddlHidden.DataValueField = "TableTime"
        ddlHidden.DataBind()

        Dim listTime As DateTime = #8:00:00#

        For i As Integer = 0 To 28
            Dim timeCheck As Integer = 0
            For j As Integer = 0 To ddlHidden.Items.Count - 1
                If listTime.ToString("HH:mm").Substring(0, 4) = ddlHidden.Items.Item(j).ToString.Substring(0, 4) Then
                    timeCheck = 1
                End If
            Next
            If timeCheck = 0 Then
                ddlTimes.Items.Add(listTime.ToString("hh:mm"))
            End If
            listTime = listTime.AddMinutes(30)
        Next

    End Sub
End Class

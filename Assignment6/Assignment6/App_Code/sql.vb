﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Public Class sql

    'Generates a parameterized insert statement. 
    'Accepts a table name an array of column names, an array Of values To be inserted, And an success message.
    Public Shared Sub insert(tableName As String, columnNames() As String, columnData() As String, message As String)
        'Database connection
        Dim conObj As conn = New conn
        Dim con As SqlConnection = conn.connect()

        'build query
        Dim buildQuery As String = "INSERT INTO "
        buildQuery &= tableName
        buildQuery &= " ("
        buildQuery &= columnNames(0)
        If columnNames.Count > 1 Then
            For i As Integer = 1 To columnNames.Count - 1
                buildQuery &= ", "
                buildQuery &= columnNames(i)
            Next
        End If
        buildQuery &= ") VALUES ("
        buildQuery &= "@p0"
        If columnData.Count > 1 Then
            For i As Integer = 1 To columnData.Count - 1
                buildQuery &= ", "
                buildQuery &= "@p" & i
            Next
        End If
        buildQuery &= ")"

        Dim cmdInsert As New SqlCommand(buildQuery, con)

        'set parameters
        cmdInsert.Parameters.Clear()

        For i As Integer = 0 To columnData.Count - 1
            cmdInsert.Parameters.AddWithValue("p" & i, columnData(i))
        Next

        'bind parameters and run query
        Try
            If con.State = ConnectionState.Closed Then con.Open()
            cmdInsert.ExecuteNonQuery()
            MsgBox(message)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try

    End Sub

    'Generates a select query  and returns a datatable. Column aliases should be passed along with the column names.
    'Accepts an array of columnt names, the table name, and an array of where clauses.
    Public Shared Function selectDT(columnNames() As String, tableName As String, whereClauses(,) As String) As DataTable
        'Database connection
        Dim conObj As conn = New conn
        Dim con As SqlConnection = conn.connect()

        'build query
        Dim buildQuery As String = "SELECT "

        buildQuery &= columnNames(0)

        If columnNames.Count > 1 Then
            For i As Integer = 1 To columnNames.Count - 1
                buildQuery &= ", "
                buildQuery &= columnNames(i)
            Next
        End If
        buildQuery &= " FROM "
        buildQuery &= tableName

        If whereClauses.GetLength(0) > 0 Then
            buildQuery &= " WHERE "
            For i As Integer = 0 To whereClauses.GetLength(0) - 1
                buildQuery &= whereClauses(i, 0)
                buildQuery &= "@p" & i & " "
            Next
        End If

        Dim cmdSelect As New SqlCommand(buildQuery, con)

        'Bind parameters and run query
        cmdSelect.Parameters.Clear()
        For i As Integer = 0 To whereClauses.GetLength(0) - 1
            cmdSelect.Parameters.AddWithValue("p" & i, whereClauses(i, 1))
        Next

        Dim daSelect As New SqlDataAdapter(cmdSelect)
        Dim dtSelect As New DataTable
        daSelect.Fill(dtSelect)

        Return dtSelect

    End Function


End Class

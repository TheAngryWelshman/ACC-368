﻿Imports Microsoft.VisualBasic

Public Class hostInfo

    Public Shared Function buildString() As String
        'get hostname
        Dim hostName As String = My.Computer.Name

        'Declare db variables
        Dim serverName As String = ""
        Dim dbName As String = ""

        'Check hostname and set db variables
        Select Case hostName
            Case "LAPTOP-43A24MUF"
                serverName = "localhost"
                dbName = "online_reservations"
            Case Else
                serverName = "COBSQL01\BISDEPT"
                dbName = "RRWEISS"
        End Select

        'Construct the connection string
        Dim conS As String = "Server=" & serverName & ";Database=" & dbName & ";Trusted_Connection=Yes;"

        Return conS

    End Function

End Class

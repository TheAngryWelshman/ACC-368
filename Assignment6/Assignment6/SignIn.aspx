﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SignIn.aspx.vb" Inherits="SignIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 202px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Username:</td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Password:</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtPassHidden" runat="server" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnContinue" runat="server" Text="Make a Reservation" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    <div>
    
        <br />
    
    </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblDate" runat="server" Text="Date" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:Calendar ID="clnReserve" runat="server" Visible="False"></asp:Calendar>
                </td>
                <td>
                    <asp:Image ID="imgSeating" runat="server" Height="335px" ImageUrl="~/Images/restaurant map.PNG" style="margin-right: 91px" Visible="False" Width="668px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblNum" runat="server" Text="Number Of People:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNumberPeople" runat="server" Visible="False"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblTableNo" runat="server" Text="Perfered Table Number:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTableNo" runat="server" Visible="False"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnShowTimes" runat="server" Text="Show Availible Times" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="lblTime" runat="server" Text="Time:" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTimes" runat="server" AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlHidden" runat="server" AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="False" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
